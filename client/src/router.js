import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import LoginForm from './views/Login.vue'
import RegistrationForm from './views/Register.vue'
import ForgotPasswordForm from './views/ForgotPassword.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: LoginForm
    },
    {
      path: '/register',
      name: 'register',
      component: RegistrationForm
    },
    {
      path: '/forgotpassword',
      name: 'forgotpassword',
      component: ForgotPasswordForm
    }//,
    // {
    //   path: '/profile',
    //   name: 'profile',
    //   component: Profile
    // }
  ]
})
